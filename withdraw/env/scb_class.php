<?php
	error_reporting(0);
	header('Content-Type: application/json');
	class Scb{
		private $deviceId = '';
		private $api_refresh = '';
		private $accnum = '';

		public function Curl($method, $url, $header, $data, $cookie)
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Android/10;FastEasy/3.54.0/5742');
			//curl_setopt($ch, CURLOPT_USERAGENT, 'okhttp/3.8.0');
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			//curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
			if ($data) {
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			}
			if ($cookie) {
				curl_setopt($ch, CURLOPT_COOKIESESSION, true);
				curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
				curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
			}
			return curl_exec($ch);
		}
		public function __construct($deviceId,$api_refresh,$accnum) {
			$this->deviceId = $deviceId;
			$this->api_refresh = $api_refresh;
			if(strlen($accnum) != 10){
				echo '10 digital !!';
			}
			$this->accnum = $accnum;
		}
		public function Login(){
			$url = "https://fasteasy.scbeasy.com:8443/v1/login/refresh";
			$headers =  array(
			"api-refresh: ".$this->api_refresh,
			"cache-control: no-cache",
			"content-type: application/json"

			);
			$data = '{ "deviceId": "'.$this->deviceId.'" }';
			$res = $this->Curl("POST",$url,$headers,$data,false);
			$res = json_decode($res,true);
			$access_token =  $res['data']['access_token'];
			if(!isset($access_token)){
				echo 'error Login';
			}
			$strFileName = "token.txt";
			$objFopen = fopen($strFileName, 'w');
			fwrite($objFopen, $access_token);





		}
		private function Access_token(){

			return file_get_contents('token.txt');
		}
		public function GetBalance(){
			$url = "https://fasteasy.scbeasy.com:8443/v2/deposits/casa/details";

			$headers =  array(
			"Api-Auth: ".$this->Access_token(),
			"content-type: application/json",
			"Accept-Language: th"
			);
			$data = '{
			"accountNo": "'.$this->accnum.'"
			}';
			$res = $this->Curl("POST",$url,$headers,$data,false);
			$d = json_decode($res,true);
			if($d['status']['code'] === "1002"){
				$this->Login();
				return $this->GetBalance();
			}
			return $res;

		}
		public function getTransaction(){
			date_default_timezone_set("Asia/Bangkok");
			$startDate = date('Y-m-d', strtotime("-5 day"));
			$endDate = date('Y-m-d', strtotime("+1 day"));
			$url = "https://fasteasy.scbeasy.com:8443/v2/deposits/casa/transactions";
			$headers =  array(
			"Api-Auth: ".$this->Access_token(),
			"content-type: application/json",
			"Accept-Language: th"
			);
			$data_scb = '{ "accountNo": "'.$this->accnum.'", "endDate": "'.$endDate.'", "pageNumber": "1", "pageSize": 50, "productType": "2", "startDate": "'.$startDate.'" }';
			$res = $this->Curl("POST",$url,$headers,$data_scb,false);
			$d = json_decode($res,true);
			if($d['status']['code'] === "1002"){
				$this->Login();
				return $this->getTransaction();
			}

			$json = json_decode($res, true);
			if (isset($json['status'])) {
				if ($json['status']['description'] === 'สำเร็จ') {
					//return $json['data']['txnList'];
					foreach($json['data']['txnList']  as $v ){
						if($v['txnCode']['code'] == 'X1'){
							preg_match_all ("/SCB x(.*) /U", $v['txnRemark'], $scbbank);
							preg_match_all ("/ ((.*)) \/X([0-9]+)([0-9]+)([0-9]+)([0-9]+)([0-9]+)([0-9]+)/U", $v['txnRemark'], $otherbank);
							$bankno = "";
							if($scbbank[0]){
								$bankno =  str_replace(" x","_",implode($scbbank[0]));
								} else {
								$bankno = str_replace("(","",str_replace(") ","_",str_replace("/X","",implode($otherbank[0]))));
							}
							//if()

							$current_bank_no = substr($bankno, strpos($bankno, "_") + 1);
							$bank_code_arr = explode('_', $bankno);
							$bank_code_en = strtolower($bank_code_arr[0]);
							$bank_code_en = trim(str_replace(' ', '', $bank_code_en));
							$Date = date("Y-m-d", strtotime($v['txnDateTime']));
							$Time = date("H:i:s", strtotime($v['txnDateTime']));
							$DateTimeCustom = date("d M Y H:i", strtotime($v['txnDateTime']));
							$TimeRemark = date("H-i-s", strtotime($v['txnDateTime']));
							$balance = (float) str_replace(',', '', $v['txnAmount']);
                			$balance_remark = str_replace('.', '', $v['txnAmount']);
							$web_remark = $Date.$TimeRemark.trim($bankno).$balance_remark;

						// 	array_push($json, array(
						// 		// "no" => $i,
						// 		"bal" => $endingBalance,
						// 		"channel" => $transCode,
						// 		"credit" => 0,
						// 		"date" => $master_date." ".$transTime,
						// 		"debit" => $transAmt,
						// 		"desc" => $transCmt
						// 	));


						// }


						// return $json;

							// $data[] = [
							// 	"datetime" => $Date." ".$Time,
							// 	"amount" => $v['txnAmount'],
							// 	"bank_no" => $current_bank_no,
							// 	"bank_code_en" => $bank_code_en,
							// 	// "description" => trim($bankno),
							// 	"txnRemark"=> $web_remark
							// ];

							$data[] = [
								"bal" => number_format((float)$v['txnAmount'], 1, '.', ''),
								"channel" => $v['txnChannel']['code'],
								"credit" => 0,
								"date" => $DateTimeCustom,
								"debit" => number_format((float)$v['txnAmount'], 1, '.', ''),
								"desc"=> rtrim($v['txnRemark'])
								// "desc"=> str_replace('   ', '', $v['txnRemark'])
							];

						}
					}
					//print_r($data);
					return $data;
					// return $json['data']['txnList'];
				}
				}else{
				echo 'Try Again';

			}

		}
		public function Verify($accountTo,$accountToBankCode,$amount){
		if($accountToBankCode == "014"){
			$transferType = "3RD";
		}else{
			$transferType = "ORFT";
		}
			$url = "https://fasteasy.scbeasy.com:8443/v2/transfer/verification";
			$headers =  array(
			"Api-Auth: ".$this->Access_token(),
			"content-type: application/json",
			"Accept-Language: th"
			);
			$data = '{
			"accountFrom": "'.$this->accnum.'",
			"accountFromType": "2",
			"accountTo": "'.$accountTo.'",
			"accountToBankCode": "'.$accountToBankCode.'",
			"amount": "'.$amount.'",
			"annotation": null,
			"transferType": "'.$transferType.'"
			}';
			$res = $this->Curl("POST",$url,$headers,$data,false);
			$d = json_decode($res,true);
			if($d['status']['code'] === "1002"){
				$this->Login();
				return $this->Verify($accountTo,$accountToBankCode,$amount);
			}
			return  $res;


		}
		public function Transfer($accountTo,$accountToBankCode,$amount){
			$Verify = $this->Verify($accountTo,$accountToBankCode,$amount);
			$Verifys = json_decode($Verify,true);
			if(!isset($Verifys['data'])){

			return $Verify;
			}
			$Verify = $Verifys['data'];
			$url = "https://fasteasy.scbeasy.com:8443/v3/transfer/confirmation";
			$headers =  array(
			"Api-Auth: ".$this->Access_token(),
			"content-type: application/json",
			"Accept-Language: th"
			);
			$data = '{
			"accountFrom": "'.$this->accnum.'",
			"accountFromName": "'.$Verify['accountFromName'].'",
			"accountFromType": "2",
			"accountTo": "'.$Verify['accountTo'].'",
			"accountToBankCode": "'.$Verify['accountToBankCode'].'",
			"accountToName": "'.$Verify['accountToName'].'",
			"amount": "'.$amount.'",
			"botFee": 0.0,
			"channelFee": 0.0,
			"fee": 0.0,
			"feeType": "",
			"pccTraceNo": "'.$Verify['pccTraceNo'].'",
			"scbFee": 0.0,
			"sequence": "'.$Verify['sequence'].'",
			"terminalNo": "'.$Verify['terminalNo'].'",
			"transactionToken": "'.$Verify['transactionToken'].'",
			"transferType": "'.$Verify['transferType'].'"
			}';
			$res = $this->Curl("POST",$url,$headers,$data,false);
			$d = json_decode($res,true);

			if($d['status']['code'] === "1002"){
				$this->Login();
				return $this->Transfer($accountTo,$accountToBankCode,$amount);
			}
			return $res;


		}


	}

//	echo $api->GetBalance();
	//$arr =  $api->getTransaction();
	//print_r($arr);

	//echo $api->Transfer('4093330353','014',1);
?>
